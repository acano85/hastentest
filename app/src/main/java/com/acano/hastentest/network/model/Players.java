package com.acano.hastentest.network.model;

import com.google.gson.annotations.SerializedName;

public class Players {
    @SerializedName("image")
    String image="";

    @SerializedName("surname")
    String surname="";

    @SerializedName("name")
    String name="";

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
