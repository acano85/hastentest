package com.acano.hastentest.network;

import com.acano.hastentest.network.model.Team;
import com.acano.hastentest.presenter.MainPresenterImp;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class GetTeamUserCase {

    private final MainPresenterImp listener;
    private Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("https://api.myjson.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    public GetTeamUserCase(MainPresenterImp listener) {
        this.listener=listener;
        callServer();
    }

    private void callServer(){
        ApiService apiService = retrofit.create(ApiService.class);
        Call<List<Team>> call = apiService.getTeams();
        call.enqueue(new Callback<List<Team>>() {
            @Override
            public void onResponse(Call<List<Team>> call, Response<List<Team>> response) {
                List<Team> teams = response.body();
                listener.onSuccess(teams);
            }

            @Override
            public void onFailure(Call<List<Team>> call, Throwable t) {
               listener.onFailure(t.getMessage());
            }
        });
    }
public interface GetTeamInterface{
       void onSuccess(List<Team> team);
       void onFailure(String message);
    }
}
