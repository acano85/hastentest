package com.acano.hastentest.network;

import com.acano.hastentest.network.model.Team;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiService {
         @GET("bins/66851")
        Call<List<Team>> getTeams();
    }
