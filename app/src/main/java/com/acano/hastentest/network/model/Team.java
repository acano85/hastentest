package com.acano.hastentest.network.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Team {
    @SerializedName("type")
    String type="";

    @SerializedName("title")
    String title="";

    @SerializedName("players")
    ArrayList<Players> players= new ArrayList<>();

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<Players> getPlayers() {
        return players;
    }

    public void setPlayers(ArrayList<Players> players) {
        this.players = players;
    }
}
