package com.acano.hastentest.ui;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.acano.hastentest.R;
import com.acano.hastentest.network.model.Players;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdapterList extends RecyclerView.Adapter<BaseViewHolder> {
    private final ArrayList<Players> list;
    private Callback mCallback;
    public static final int VIEW_TYPE_EMPTY = 0;
    public static final int VIEW_TYPE_NORMAL = 1;


    public AdapterList(ArrayList<Players> list) {
        this.list = list;
    }
    public void setCallback(Callback callback) {
        mCallback = callback;
    }
    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }
    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                return new ViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.row, parent, false));
            case VIEW_TYPE_EMPTY:
            default:
                return new EmptyViewHolder(
                        LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.item_empty_view, parent, false));
        }
    }
    @Override
    public int getItemViewType(int position) {
        if (list != null && list.size() > 0) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }
    @Override
    public int getItemCount() {
        if (list != null && list.size() > 0) {
            return list.size();
        } else {
            return 1;
        }
    }
    public void addItems(ArrayList<Players> sportList) {
        list.addAll(sportList);
        notifyDataSetChanged();
    }
    public interface Callback {
        void onEmptyViewRetryClick();
    }


    /** ViewHolder Class */
    public class ViewHolder extends BaseViewHolder {
        @BindView(R.id.IvImage)
        ImageView coverImageView;
        @BindView(R.id.TvName)
        TextView titleTextView;
        @BindView(R.id.TvSurname)
        TextView newsTextView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
        protected void clear() {
            coverImageView.setImageDrawable(null);
            titleTextView.setText("");
            newsTextView.setText("");
        }
        public void onBind(int position) {
            super.onBind(position);
            final Players mSport = list.get(position);
            if (mSport.getImage() != null) {
                Glide.with(itemView.getContext())
                        .load(mSport.getImage())
                        .error(R.drawable.android_mask)
                        .transform(new CircleCrop())
                        .into(coverImageView);
            }
            if (mSport.getName() != null) {
                titleTextView.setText(mSport.getName());
            }
            if (mSport.getSurname() != null) {
                newsTextView.setText(mSport.getSurname());
            }

        }
    }


public class EmptyViewHolder extends BaseViewHolder {
    @BindView(R.id.tv_message)
    TextView messageTextView;
    @BindView(R.id.buttonRetry)
    TextView buttonRetry;
    EmptyViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        buttonRetry.setOnClickListener(v -> mCallback.onEmptyViewRetryClick());
    }
    @Override
    protected void clear() {
    }
}

}
