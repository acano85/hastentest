package com.acano.hastentest.ui;

import com.acano.hastentest.network.model.Players;

import java.util.ArrayList;

public interface MainView {

    void showErrorMessage(String message);

    void setPlayersList(ArrayList<Players> players);
}
