package com.acano.hastentest.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.acano.hastentest.R;
import com.acano.hastentest.network.model.Players;
import com.acano.hastentest.presenter.MainPresenterImp;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements MainView{


    private AdapterList mSportAdapter;
    private RecyclerView rvView;
    private MainPresenterImp mainPresenterImp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mainPresenterImp = new MainPresenterImp(this);
        mainPresenterImp.userEnter();
        setUp();

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Toast.makeText(this, "Selected Item: " +item.getTitle(), Toast.LENGTH_SHORT).show();
        switch (item.getItemId()) {
            case R.id.footbal:
                mainPresenterImp.optionSelected(MainPresenterImp.FOOTBALL);
            return true;
            case R.id.tennis:
                mainPresenterImp.optionSelected(MainPresenterImp.TENNIS);
                return true;
            case R.id.golf:
                mainPresenterImp.optionSelected(MainPresenterImp.GOLF);
                return true;
            case R.id.motor:
                mainPresenterImp.optionSelected(MainPresenterImp.F1);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    private void setUp() {
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

        rvView = (RecyclerView) findViewById(R.id.rvView);
        rvView.setLayoutManager(layoutManager);
    }

    private void refresh() {
        mainPresenterImp.optionSelected(MainPresenterImp.FOOTBALL);
    }

    @Override
    public void showErrorMessage(String message) {
        Toast.makeText(this,message,Toast.LENGTH_LONG).show();
    }

    @Override
    public void setPlayersList(ArrayList<Players> players) {
        mSportAdapter=null;
        mSportAdapter= new AdapterList(players);
        rvView.setAdapter(mSportAdapter);
        mSportAdapter.setCallback(this::refresh);
    }
}
