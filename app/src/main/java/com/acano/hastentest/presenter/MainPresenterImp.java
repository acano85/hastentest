package com.acano.hastentest.presenter;

import com.acano.hastentest.ui.MainView;
import com.acano.hastentest.network.GetTeamUserCase;
import com.acano.hastentest.network.model.Team;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

public class MainPresenterImp implements MainPresenter, GetTeamUserCase.GetTeamInterface {
    public static final int FOOTBALL=0;
    public static final int TENNIS=1;
    public static final int GOLF=2;
    public static final int F1=3;


    private  MainView view;
    private List<Team> teams;

    public MainPresenterImp(MainView view) {
        this.view=view;
    }

    @Override
    public void userEnter() {
        new GetTeamUserCase(this);
    }

    @Override
    public void optionSelected(int opt) {
        int optionInt=0;

        if(teams!=null) {
            for (Team team : teams) {
                if (!(team.getTitle().equals("Football") || team.getTitle().equals("Tennis") || team.getTitle().equals("Golf") || team.getTitle().equals("Formula 1"))) {
                    optionInt = -1;
                } else {
                    if (team.getTitle().equals("Football") && opt == FOOTBALL) {
                        optionInt = FOOTBALL;
                        break;
                    } else if (team.getTitle().equals("Tennis") && opt == TENNIS) {
                        optionInt = TENNIS;
                        break;
                    } else if (team.getTitle().equals("Golf") && opt == GOLF) {
                        optionInt = GOLF;
                        break;
                    } else if (team.getTitle().equals("Formula 1") && opt == F1) {
                        optionInt = F1;
                        break;
                    }
                }
            }
            if (optionInt == -1) {
                view.setPlayersList(new ArrayList<>());
            } else {
                view.setPlayersList(teams.get(optionInt).getPlayers());
            }
        }
        else {
            view.setPlayersList(new ArrayList<>());
            new GetTeamUserCase(this);
        }
    }

    @Override
    public void onSuccess(List<Team> teams) {
        this.teams=teams;

        view.setPlayersList(teams.get(FOOTBALL).getPlayers());
    }

    @Override
    public void onFailure(String message) {
        view.showErrorMessage(message);

    }

}
