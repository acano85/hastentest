package com.acano.hastentest.presenter;

public interface MainPresenter {

    void userEnter();

    void optionSelected(int opt);
}
